---
title: STP配置边缘端口的两种方式（华为交换机）
date: 2020-03-05 20:57:32
categories:
  - tech
tags:
  - stp
---

## 方式 1

先分别在每台交换机配置(所有端口为)边缘端口

```bash
[系统视图] stp edged-port default
```

然后手动把交换机互联端口取消边缘端口

```bash
[接口视图] stp edged-port disable
```

> 理论上讲，并不需要手动取消边缘端口，端口配置成边缘端口后，如果收到 BPDU 报文，交换设备会自动将边缘端口设置为非边缘端口，并重新进行生成树计算，
> 但它只会识别收到的报文，根桥的指定端口不会收到 BPDU 报文，所以根桥/备份根桥的这些端口会不会自动被设为非边缘端口，所以我们需要使用`stp edged-port disable`手动把根桥&备份根桥的互联端口设为非边缘端口

## 方式二

手动配置每个不参与 STP 计算的端口

```bash
[接口视图] stp edged-port enable
```

> 此种方式配置的边缘端口如想取消，可以用`undo stp edged-port`也可以用`stp edged-port disable`
