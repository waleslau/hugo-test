---
title: 「PVE」配置虚拟机使其支持 xterm.js
date: 2020-08-01 15:25:17
categories:
  - tech
tags:
  - linux
  - pve
---

## 前言

软件版本：PVE 6.2

虽然 PVE 支持使用 xterm.js 连接到虚拟机，但我发现目前是无法直接使用这个功能的，必须要配置一下

## 1. 关闭 VM，然后在 PVE Host 中为虚拟机添加串行端口

可以使用 GUI，也可以使用命令行

- GUI 方式：

`虚拟机 > 硬件 > 添加 > 添加串行端口 > 添加(端口号默认就 OK)`

![1.1 选中‘添加串行端口’](/img/posts/PVE-connect-to-the-virtual-machine-by-xterm-js.assets/image-20200801143644825.png)

![1.2 添加端口，端口号默认即可](/img/posts/PVE-connect-to-the-virtual-machine-by-xterm-js.assets/image-20200801143942007.png)

- 命令行方式

```bash
qm set <VMiD> -serial0 socket
```

## 2. 重新启动 VM，进入虚拟机中修改配置

先用 dmesg 命令确认是否有 ttyS 出现，

```bash
dmesg | grep ttyS
```

![dmesg](/img/posts/PVE-connect-to-the-virtual-machine-by-xterm-js.assets/image-20200801145027472.png)

编辑文件 `/etc/default/grub` ，把 `GRUB_CMDLINE_LINUX` 这一项修改为下面内容

```bash
GRUB_CMDLINE_LINUX="quiet console=tty0 console=ttyS0,115200"
```

然后更新调整后的 grub 设置以生效

## 3. 重启 VM，连接到串行终端

然后你可以使用 xterm.js 连接到虚拟机

![xterm.js](/img/posts/PVE-connect-to-the-virtual-machine-by-xterm-js.assets/image-20200801150509404.png)

如果显示下面内容，敲一下回车键就行了

![xterm.js](/img/posts/PVE-connect-to-the-virtual-machine-by-xterm-js.assets/image-20200801150643035.png)

然后输入用户名和密码登录

![xterm.js](/img/posts/PVE-connect-to-the-virtual-machine-by-xterm-js.assets/image-20200801150746710.png)

也可以在 PVE Host 中执行下面命令连接到虚拟机

```bash
qm terminal <VMiD>
```

---

更多内容详见：

[https://pve.proxmox.com/wiki/Serial_Terminal](https://pve.proxmox.com/wiki/Serial_Terminal)
