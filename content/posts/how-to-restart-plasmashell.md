---
title: 如何重启KDE桌面
date: 2022-02-27 16:18:27
categories:
  - tech
tags:
  - linux
  - kde
---

在终端中输入如下命令并回车

```bash
kquitapp5 plasmashell && kstart plasmashell
```

也可以 alt + F2 启动 KRunner 直接输入上面命令
